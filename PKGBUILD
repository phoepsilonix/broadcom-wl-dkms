# Maintainer: Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor: Andrey Vihrov <andrey.vihrov@gmail.com>
# Contributor: Frank Vanderham <twelve.eighty@gmail.com>
# Contributor: Jacob McSwain <jacob.a.mcswain@gmail.com>
# Contributor: Thomas Braby <thomas@mykolab.com>

pkgname=broadcom-wl-dkms
pkgver=6.30.223.271
pkgrel=30
pkgdesc='Broadcom 802.11 Linux STA wireless driver'
arch=(x86_64)
url='https://www.broadcom.com/support/download-search/?pf=Wireless+LAN+Infrastructure'
license=(custom)
depends=(dkms)
conflicts=(broadcom-wl)
install=broadcom-wl-dkms.install
source=('broadcom-wl-dkms.conf'
        'dkms.conf.in'
        '001-null-pointer-fix.patch'
        '002-rdtscl.patch'
        '003-linux47.patch'
        '004-linux48.patch'
        '005-debian-fix-kernel-warnings.patch'
        '006-linux411.patch'
        '007-linux412.patch'
        '008-linux415.patch'
        '009-fix_mac_profile_discrepancy.patch'
        '010-linux56.patch'
        '011-linux59.patch'
        '012-linux517.patch'
        '013-Makefile.patch'
        '014-brcm-wlan-ifname.patch'
        "https://docs.broadcom.com/docs-and-downloads/docs/linux_sta/hybrid-v35_64-nodebug-pcoem-${pkgver//./_}.tar.gz")
sha256sums=('b97bc588420d1542f73279e71975ccb5d81d75e534e7b5717e01d6e6adf6a283'
            'bf16df09fd015bb282a27d89d5ed13bbd10eb847d5cde605dc3f92284430a266'
            '32e505a651fdb9fd5e4870a9d6de21dd703dead768c2b3340a2ca46671a5852f'
            '4ea03f102248beb8963ad00bd3e36e67519a90fa39244db065e74038c98360dd'
            '30ce1d5e8bf78aee487d0f3ac76756e1060777f70ed1a9cf95215c3a52cfbe2e'
            '09d709df0c764118ca43117f5c096163d9669a28170da8476d4b8211bd225d2e'
            '2306a59f9e7413f35a0669346dcd05ef86fa37c23b566dceb0c6dbee67e4d299'
            '5bc12cb57712e6a944dff1c90de50135c2508085d8497ab99284ccccdb35c32b'
            'a3d13e8abb96ad440dbfae29acae82d31d1ced2ea62052f1efb2c3c4add347ce'
            '08c24157cf3b93b60e67e600d1d90223447361990df09acfb00281d79813d167'
            '4e73e50653bb612946edd34bf31ca5a0b80f632d47a08766ae6042880927c98d'
            'f1300bcce93363088481671150ff2bbd6957e12ba11098980b9f428c7a171812'
            '51ab3d327ec0800106d09678bc51356ec22a2190b1bff8743f8d56b1f83f10c4'
            'cf118da7d7a091dc6be01e9dc4b5302c81c416b2e147e87d37c9dd4c6dc1b6bd'
            '965c7075afc7cdec61dc6555122a745f9f4e13260343a8eea51feffe4283996b'
            'c5916f5e5c18b36b3a6507b4f111cacc4f34c727b29b99615f76405354521da0'
            '5f79774d5beec8f7636b59c0fb07a03108eef1e3fd3245638b20858c714144be')

prepare() {
  sed -e "s/@PACKAGE_VERSION@/$pkgver/" dkms.conf.in > dkms.conf
}

package() {
  local dest="$pkgdir/usr/src/${pkgname/-dkms/}-$pkgver"

  install -Dm644 Makefile "$dest/Makefile"
  install -Dm644 dkms.conf "$dest/dkms.conf"
  install -Dm644 -t "$dest/patches" *.patch
  cp -a src "$dest"

  install -Dm644 lib/wlc_hybrid.o_shipped \
    "$pkgdir/usr/lib/$pkgname/wlc_hybrid.o_shipped"
  install -Dm644 broadcom-wl-dkms.conf \
    "$pkgdir/usr/lib/modprobe.d/broadcom-wl-dkms.conf"
  install -Dm644 lib/LICENSE.txt \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
